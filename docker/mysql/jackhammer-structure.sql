-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: jackhammer
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `adr_id` int(11) NOT NULL AUTO_INCREMENT,
  `mcc_id` int(11) NOT NULL,
  `adr_hdkey` varchar(64) NOT NULL,
  `adr_requested_amount` decimal(34,18) unsigned DEFAULT NULL,
  `adr_requested_amount_usd` decimal(18,2) unsigned DEFAULT NULL,
  `adr_rate_usd` decimal(12,2) unsigned DEFAULT NULL,
  `adr_address` varchar(255) NOT NULL,
  `adr_required_confirmations` smallint(6) unsigned NOT NULL,
  `adr_callback_url` text,
  `adr_custom_data` varchar(255) DEFAULT NULL,
  `adr_is_persistent` tinyint(1) NOT NULL,
  `adr_is_exported` tinyint(1) NOT NULL,
  `adr_created_at_height` int(11) unsigned NOT NULL,
  `adr_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`adr_id`),
  UNIQUE KEY `adr_address` (`adr_address`),
  UNIQUE KEY `adr_hdkey` (`adr_hdkey`),
  KEY `mcc_id` (`mcc_id`),
  CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`mcc_id`) REFERENCES `merchants_currencies` (`mcc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `cur_id` int(11) NOT NULL,
  `blk_height` int(11) unsigned NOT NULL,
  PRIMARY KEY (`cur_id`,`blk_height`) USING BTREE,
  CONSTRAINT `blocks_ibfk_1` FOREIGN KEY (`cur_id`) REFERENCES `currencies` (`cur_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `callbacks`
--

DROP TABLE IF EXISTS `callbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callbacks` (
  `cbk_id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_id` int(11) NOT NULL,
  `cbk_confirmations` int(11) NOT NULL,
  `cbk_status` tinyint(4) NOT NULL,
  `cbk_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cbk_id`),
  KEY `tx_id` (`tx_id`),
  CONSTRAINT `callbacks_ibfk_1` FOREIGN KEY (`tx_id`) REFERENCES `transactions` (`tx_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `cur_id` int(11) NOT NULL AUTO_INCREMENT,
  `cur_type` int(11) NOT NULL,
  `cur_code` varchar(32) NOT NULL,
  `cur_title` varchar(128) NOT NULL,
  `cur_is_enabled` tinyint(1) NOT NULL,
  `cur_sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`cur_id`),
  UNIQUE KEY `cur_code` (`cur_code`),
  UNIQUE KEY `cur_title` (`cur_title`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flushes`
--

DROP TABLE IF EXISTS `flushes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flushes` (
  `fsh_id` int(11) NOT NULL AUTO_INCREMENT,
  `adr_id` int(11) NOT NULL,
  `fsh_tx_body` blob,
  `fsh_tx_signatures` text,
  `fsh_status` tinyint(4) NOT NULL,
  `fsh_tx_hash` text,
  `fsh_fund_tx_hash` text,
  `fsh_eth_txcost` varchar(32) DEFAULT NULL,
  `fsh_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fsh_id`),
  KEY `adr_id` (`adr_id`),
  CONSTRAINT `flushes_ibfk_1` FOREIGN KEY (`adr_id`) REFERENCES `addresses` (`adr_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ignored_addresses`
--

DROP TABLE IF EXISTS `ignored_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ignored_addresses` (
  `ign_id` int(11) NOT NULL AUTO_INCREMENT,
  `mch_id` int(11) NOT NULL,
  `ign_address` varchar(255) NOT NULL,
  `ign_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ign_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ign_id`),
  UNIQUE KEY `mch_id_2` (`mch_id`,`ign_address`),
  CONSTRAINT `ignored_addresses_ibfk_1` FOREIGN KEY (`mch_id`) REFERENCES `merchants` (`mch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchants`
--

DROP TABLE IF EXISTS `merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchants` (
  `mch_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `mch_title` varchar(128) DEFAULT NULL,
  `mch_api_key` varchar(128) NOT NULL,
  `mch_secret_key` varchar(128) NOT NULL,
  `mch_hdkey` varchar(64) NOT NULL,
  `mch_is_persistent_address` tinyint(1) NOT NULL,
  `mch_is_enabled` tinyint(1) NOT NULL,
  `mch_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mch_id`),
  UNIQUE KEY `mch_key` (`mch_api_key`),
  UNIQUE KEY `mch_secret` (`mch_secret_key`),
  UNIQUE KEY `mch_hdkey` (`mch_hdkey`),
  KEY `usr_id` (`usr_id`),
  CONSTRAINT `merchants_ibfk_1` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchants_currencies`
--

DROP TABLE IF EXISTS `merchants_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchants_currencies` (
  `mcc_id` int(11) NOT NULL AUTO_INCREMENT,
  `mch_id` int(11) NOT NULL,
  `cur_id` int(11) NOT NULL,
  `mcc_is_enabled` tinyint(1) NOT NULL,
  `mcc_flush_address` varchar(255) NOT NULL,
  `mcc_required_signers` tinyint(11) unsigned NOT NULL,
  `mcc_signers` text NOT NULL,
  `mcc_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mcc_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mcc_id`),
  UNIQUE KEY `mch_id` (`mch_id`,`cur_id`) USING BTREE,
  KEY `cur_id` (`cur_id`),
  CONSTRAINT `merchants_currencies_ibfk_1` FOREIGN KEY (`mch_id`) REFERENCES `merchants` (`mch_id`) ON UPDATE CASCADE,
  CONSTRAINT `merchants_currencies_ibfk_2` FOREIGN KEY (`cur_id`) REFERENCES `currencies` (`cur_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` bigint(20) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `tx_id` int(11) NOT NULL AUTO_INCREMENT,
  `adr_id` int(11) NOT NULL,
  `cur_id` int(11) NOT NULL,
  `fsh_id` int(11) DEFAULT NULL,
  `tx_hash` varchar(255) NOT NULL,
  `tx_amount` decimal(34,18) unsigned DEFAULT NULL,
  `tx_confirmations` int(11) NOT NULL,
  `tx_memo` text,
  `tx_out` int(11) NOT NULL DEFAULT '0',
  `tx_status` tinyint(4) NOT NULL,
  `tx_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tx_id`),
  UNIQUE KEY `tx_hash` (`tx_hash`),
  KEY `adr_id` (`adr_id`),
  KEY `fsh_id` (`fsh_id`),
  KEY `cur_id` (`cur_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`adr_id`) REFERENCES `addresses` (`adr_id`) ON UPDATE CASCADE,
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`fsh_id`) REFERENCES `flushes` (`fsh_id`) ON UPDATE CASCADE,
  CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`cur_id`) REFERENCES `currencies` (`cur_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transfers`
--

DROP TABLE IF EXISTS `transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfers` (
  `trn_id` int(11) NOT NULL AUTO_INCREMENT,
  `cur_id` int(11) NOT NULL,
  `trn_from` varchar(255) NOT NULL,
  `trn_to` varchar(255) NOT NULL,
  `trn_amount` decimal(34,18) NOT NULL,
  `trn_blocknum` int(10) unsigned NOT NULL,
  `trn_txhash` varchar(255) NOT NULL,
  `trn_logindex` int(11) NOT NULL,
  PRIMARY KEY (`trn_id`),
  UNIQUE KEY `trn_txhash` (`trn_txhash`,`trn_logindex`),
  KEY `cur_id` (`cur_id`),
  CONSTRAINT `transfers_ibfk_1` FOREIGN KEY (`cur_id`) REFERENCES `currencies` (`cur_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_email` varchar(255) NOT NULL,
  `usr_password_hash` text NOT NULL,
  `usr_verification_key` varchar(128) NOT NULL,
  `usr_is_verified` tinyint(1) NOT NULL,
  `usr_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `usr_email` (`usr_email`),
  UNIQUE KEY `usr_verification_key` (`usr_verification_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-04 12:18:04
