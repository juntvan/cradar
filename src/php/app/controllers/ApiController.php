<?php

namespace App\Controllers;


use App\Models\Transfers;
use Bhaktaraz\RSSGenerator;

class ApiController extends ControllerBase
{
    public function jsonAction()
    {
        $trns = $this->getTransfers();
        $data = [];
        foreach ($trns as $trn) {
            $node = [
                'block' => $trn->trn_blocknum,
                'amount' => round($trn->trn_amount, 4),
                'code' => $trn->cur->cur_code,
                'time' => $trn->trn_blocktime,
                'hash' => $trn->trn_txhash,
                'from' => $trn->trn_from,
                'to' => $trn->trn_to,
                'title' => $trn->cur->cur_title,
            ];
            array_push($data, $node);
        }

        return $this->response->setJsonContent($data);
    }

    public function rssAction()
    {
        $feed = new RSSGenerator\Feed();
        $channel = new RSSGenerator\Channel();
        $channel
            ->title("Transactions")
            ->url('/api/rss')
            ->description('List of available parametrs ')
            ->language('en-US')
            ->appendTo($feed);

        $trns = $this->getTransfers();
        foreach ($trns as $trn) {

            $title = implode(' | ',[$trn->cur->cur_code,round($trn->trn_amount,5),$trn->trn_blocktime]);
            $desc = "From: {$trn->trn_from} \t To:{$trn->trn_to} Token:{$trn->cur->cur_code}";

            $item = new RSSGenerator\Item();
            $item
                ->title($title)
                ->url("https://etherscan.io/tx/$trn->trn_txhash")
                ->creator($trn->trn_from)
                ->category($trn->trn_txhash)
                ->enclosure("https://etherscan.io/address/{$trn->trn_to}",0,'text/html')
                ->content($trn->trn_to)
                ->pubDate(strtotime($trn->trn_blocktime))
                ->description($desc)
                ->appendTo($channel);
        }
        return $this->response->setContentType('application/xml')->setContent($feed);
    }

    private function getTransfers()
    {
        $limit = intval($this->request->get('limit'));
        if ($limit < 1 or $limit > 1000) {
            $limit = 100;
        }

        $query = Transfers::query()
            ->innerJoin('App\Models\Currencies', 'App\Models\Transfers.cur_id=cur.cur_id', 'cur')
            ->orderBy('trn_blocknum DESC')
            ->limit($limit);

        $trn_hash = $this->request->get('hash');
        if (!empty($trn_hash)) {
            $query->andWhere('trn_txhash = :hash:', ["hash" => $trn_hash]);
        }

        $trn_code = $this->request->get('code');
        if (!empty($trn_code)) {
            $query->andWhere('cur_code = :code:', ["code" => $trn_code]);
        }

        try {
            $trns = $query->execute();
        } catch (\Exception $e) {
            var_dump($e);
            exit();
        }
        return $trns;
    }
}