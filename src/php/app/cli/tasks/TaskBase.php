<?php

class TaskBase extends Phalcon\CLI\Task
{
    private $proto;

    protected function telegramLogin()
    {
        if(!empty($this->proto)){
            return $this->proto;
        }


        $settings =  [
            'app_info' => ['api_id' => $this->config->telegram->api->id, 'api_hash' => $this->config->telegram->api->hash],
            'updates' => ['handle_updates' => false],
        ];

        $this->proto = new \danog\MadelineProto\API('session.madeline', $settings);

        $this->proto->start();

        return $this->proto;
    }
}