<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class Transfers extends ModelBase
{
	public function initialize()
	{
		$this->belongsTo('cur_id','App\Models\Currencies','cur_id',['alias'=>'cur']);
	}
}